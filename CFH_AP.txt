Due Date:
	Aggro|Passive Speaks
	Object Scan Zoom
	Scan Sequence			03/28



CFH Action Plan:

Story:
	Outline flow given components
	Write next revision

Tools:
	Level Streaming
	Subtitle/Audio Menu
	

Interactive Dialogue System:
	Refactor Code
		Add
	HUD:
		If NPC notice object, track it
		Scan animation should be sequence var
		On screen location tracker
		Scan animation and object zoom
		
Level Creation:
	Arena collision juttering
	Finish hydro offices
		Ivy decoration? Texture properly, map lab and office
	
Substance:
	Tutorial to fully learn
	Properly texture opening scene
	
Blender:
	Second rev of opening assets
	
Animation:
	Creation of character arms and animate scenarios:
		Drill locks
		Connect wires
		Play audio clip?
	Create alliens and animations:
		Parent:
			Walk (Change rate for aggressive or not?)
			Attack sequence
		Child:
			Defensive stance
			Jump and face hug
			
			
To Ship DST:
	Basic character model
		Arm Drill and suit look
	HUD redo object tracking and zoom (maybe zoom)
	Touch up animations
	Full test of communication system
	Story:
		Work through story cycle for characters
		First Draft
	Monitor dialogue selection for feedback
	Finish office layout and build