// Fill out your copyright notice in the Description page of Project Settings.


#include "MyGameInstanceCpp.h"
#include "ActionObjectModel.h"
#include "Engine/DataTable.h"
#include "Materials/MaterialParameterCollection.h"


UMyGameInstanceCpp::UMyGameInstanceCpp(const FObjectInitializer& ObjectInitializer)//: Super(ObjectInitializer)
{
	ActionGuideDT = GetActionGuideDT();
}

UDataTable* UMyGameInstanceCpp::GetActionGuideDT()
{
	static ConstructorHelpers::FObjectFinder<UDataTable> ActionGuideDTObject(TEXT("DataTable'/Game/MyContent/Audio/CFH_ActionGuide.CFH_ActionGuide'"));
	if (ActionGuideDTObject.Succeeded())
	{
		return ActionGuideDTObject.Object;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT(""));
		return nullptr;
	}
}

FActionGuide* UMyGameInstanceCpp::GetActionObjectSequence(FName ActionObjectName)
{
	FActionGuide* ActionObjectSequence = ActionGuideDT->FindRow<FActionGuide>(ActionObjectName, ContextString);

	return ActionObjectSequence;
}