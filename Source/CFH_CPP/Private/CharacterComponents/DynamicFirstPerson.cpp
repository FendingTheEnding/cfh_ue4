// Kenneth Kratzer 2020


#include "DynamicFirstPerson.h"
#include "Camera/CameraComponent.h"
#include "CFH_AnimInstance.h"
#include "CFH_CharacterActionHandler.h"
#include "GameFramework/Character.h"
//#include "GameFramework/PlayerController.h"
//#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UDynamicFirstPerson::UDynamicFirstPerson()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDynamicFirstPerson::BeginPlay()
{
	Super::BeginPlay();

	// ...

	if (GetAndCheckRequiredClassPointers())
	{
		GetInitialRotationDifference();
		SetupInputComponent();
	}
}

bool UDynamicFirstPerson::GetAndCheckRequiredClassPointers()
{
	MyCharacter = Cast<ACharacter>(GetOwner());
	CharacterMesh = MyCharacter->GetMesh();
	AnimInstance = Cast<UCFH_AnimInstance>(MyCharacter->GetMesh()->GetAnimInstance());
	CharacterActionHandler = Cast<UCFH_CharacterActionHandler>(MyCharacter->FindComponentByClass<UCFH_CharacterActionHandler>());
	if (MyCharacter && AnimInstance && CharacterActionHandler)
	{
		return true;
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("DynamicFirstPerson.cpp: MyCharacter or AnimInstance not assigned"));
		return false;
	}
}

void UDynamicFirstPerson::GetInitialRotationDifference()
{
	InitialRotationDifference = MyCharacter->GetMesh()->GetComponentRotation().Yaw - MyCharacter->FindComponentByClass<UCameraComponent>()->GetComponentRotation().Yaw;
	if (InitialRotationDifference < 0)
	{
		InitialRotationDifference += 360;
	}
}


// Called every frame
void UDynamicFirstPerson::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	Speed = MyCharacter->GetVelocity().Size();
}

void UDynamicFirstPerson::SetupInputComponent()
{
	UInputComponent* InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();
	RemoveInputComponentsToBeReplaced(InputComponent);
	AssignNewInputComponents(InputComponent);

	UE_LOG(LogTemp, Warning, TEXT("Axis Re-Bound"))
}

void UDynamicFirstPerson::RemoveInputComponentsToBeReplaced(UInputComponent* InputComponent)
{
	for (FName BindAxis : AxisToBind)
	{
		for (int32 Inc = 0; Inc < InputComponent->AxisBindings.Num(); Inc++)
		{
			if (InputComponent->AxisBindings[Inc].AxisName == BindAxis)
			{
				InputComponent->AxisBindings.RemoveAt(Inc);
			}
		}
	}
}

void UDynamicFirstPerson::AssignNewInputComponents(UInputComponent* InputComponent)
{
	InputComponent->BindAxis("Turn", this, &UDynamicFirstPerson::TurnAtRate);
	InputComponent->BindAxis("TurnRate", this, &UDynamicFirstPerson::TurnAtRate);
	InputComponent->BindAxis("LookUp", this, &UDynamicFirstPerson::LookUpAtRate);
	InputComponent->BindAxis("LookUpRate", this, &UDynamicFirstPerson::LookUpAtRate);
}

void UDynamicFirstPerson::TurnAtRate(float Rate)
{
	float RotationValue = Rate * (BaseTurnRate)*GetWorld()->GetDeltaSeconds();
	// Used to measure viewport yaw movement
	SummationOfDegreesMoved += RotationValue;

	// If in an animation
	if (AnimInstance->GetActionAnim() == EActionAnim::Active)
	{
		RotationLockedAddControllerYawInput(RotationValue);
		CharacterMesh->SetWorldLocation(AnimLockedCharacterLocation);
		CharacterMesh->SetWorldRotation(AnimLockedCharacterRotation);
	}
	else if (!CharacterActionHandler->IsMoveForAnimActive())
	{
		MyCharacter->AddControllerYawInput(RotationValue);
		RotateMeshCounterToCamera(RotationValue);
	}
}

void UDynamicFirstPerson::RotationLockedAddControllerYawInput(float RotationValue)
{
	float NewYaw = MyCharacter->GetControlRotation().Yaw + RotationValue;

	//UE_LOG(LogTemp, Warning, TEXT("NewYaw: %f, AnimYawMax: %f, AnimYawMin: %f"), NewYaw, AnimYawMax, AnimYawMin)
	if (IsNewYawWithinBounds(NewYaw))
	{
		MyCharacter->AddControllerYawInput(RotationValue);
	}
	else
	{
		// rewire from neg/pos to increase/decrease use CharRot+Offset
		AddYawInputUnderBoundedConditions(NewYaw, RotationValue);
	}
}

bool UDynamicFirstPerson::IsNewYawWithinBounds(float NewYaw)
{
	return (NewYaw < AnimYawMax && NewYaw > AnimYawMin);
}

void UDynamicFirstPerson::AddYawInputUnderBoundedConditions(float NewYaw, float RotationValue)
{
	if (IsBoundsValueWithin0to360())
	{
		if (NewYaw < AnimYawMin && RotationValue > 0)
		{
			MyCharacter->AddControllerYawInput(RotationValue);
		}
		else if (NewYaw > AnimYawMax && RotationValue < 0)
		{
			MyCharacter->AddControllerYawInput(RotationValue);
		}
	}
	else
	{
		// UE_LOG(LogTemp, Warning, TEXT("%f"), RotationValue);
		if (IsCurrentYawGreaterThan180())
		{
			if (NewYaw > AnimYawMin && RotationValue < 0)
			{
				MyCharacter->AddControllerYawInput(RotationValue);
			}
			else if (RotationValue > 0)
			{
				MyCharacter->AddControllerYawInput(RotationValue);
			}
		}
		else
		{
			if (NewYaw < AnimYawMax && RotationValue > 0)
			{
				MyCharacter->AddControllerYawInput(RotationValue);
			}
			else if (RotationValue < 0)
			{
				MyCharacter->AddControllerYawInput(RotationValue);
			}
		}
	}
}

bool UDynamicFirstPerson::IsBoundsValueWithin0to360()
{
	return (!AnimMaxFlipped && !AnimMinFlipped);
}

bool UDynamicFirstPerson::IsCurrentYawGreaterThan180()
{
	return (MyCharacter->GetControlRotation().Yaw > 180);
}

void UDynamicFirstPerson::RotateMeshCounterToCamera(float RotationValue)
{
	float RotationDifference = CharacterMesh->GetComponentRotation().Yaw - MyCharacter->FindComponentByClass<UCameraComponent>()->GetComponentRotation().Yaw;

	RotationDifference = NormalizeAngle0to360(RotationDifference);
	if (ShouldMeshRotateToCameraCenter()) {
		AddRotationToCenterMesh(RotationDifference);
		if (IsMeshCentered(RotationDifference))
		{
			SummationOfDegreesMoved = 0;
		}
	}
	else {
		if (RotationDifference < InitialRotationDifference + 25.5 && RotationDifference > InitialRotationDifference - 25.5) {
			CharacterMesh->AddRelativeRotation({ 0, -RotationValue, 0 });
		}
	}
}

float UDynamicFirstPerson::NormalizeAngle0to360(float Angle)
{
	if (Angle < 0)
	{
		Angle += 360;
	}
	else if (Angle > 360)
	{
		Angle -= 360;
	}
	return Angle;
}

bool UDynamicFirstPerson::ShouldMeshRotateToCameraCenter()
{
	return (abs(SummationOfDegreesMoved) > 25.f || Speed != 0);
}

void UDynamicFirstPerson::AddRotationToCenterMesh(float RotationDifference)
{
	if (RotationDifference > InitialRotationDifference + 0.5f) {
		CharacterMesh->AddRelativeRotation({ 0,-abs(2.f * (0.35f * (BaseTurnRate)*GetWorld()->GetDeltaSeconds())),0 });
		//AnimInst->SetTurnAnim(0);
	}
	else if (RotationDifference < InitialRotationDifference - 0.5f) {
		CharacterMesh->AddRelativeRotation({ 0,abs(2.f * (0.35f * (BaseTurnRate)*GetWorld()->GetDeltaSeconds())),0 });
		//AnimInst->SetTurnAnim(1);
	}
}

bool UDynamicFirstPerson::IsMeshCentered(float RotationDifference)
{
	return (RotationDifference < InitialRotationDifference + 0.5f && RotationDifference > InitialRotationDifference - 0.5f);
}

void UDynamicFirstPerson::LookUpAtRate(float Rate)
{

	// calculate delta for this frame from the rate information
	float RotationValue = Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds();

	// If in an animation
	if (AnimInstance->GetActionAnim() == EActionAnim::Active)
	{
		float NewPitch = MyCharacter->GetControlRotation().GetNormalized().Pitch - RotationValue;
		if (IsNewPitchWithinBounds(NewPitch))
		{
			MyCharacter->AddControllerPitchInput(RotationValue);
		}
		else
		{
			AddPitchInputUnderboundedConditions(NewPitch, RotationValue);
		}
	}
	else if (!CharacterActionHandler->IsMoveForAnimActive()) {
		MyCharacter->AddControllerPitchInput(RotationValue);
	}
}

bool UDynamicFirstPerson::IsNewPitchWithinBounds(float NewPitch)
{
	return (NewPitch < AnimPitchMax && NewPitch > AnimPitchMin);
}

void UDynamicFirstPerson::AddPitchInputUnderboundedConditions(float NewPitch, float RotationValue)
{
	if (NewPitch < AnimPitchMin + 20.f && RotationValue < 0)
	{
		MyCharacter->AddControllerPitchInput(RotationValue);
	}
	else if (NewPitch > AnimPitchMax - 20.f && RotationValue > 0)
	{
		MyCharacter->AddControllerPitchInput(RotationValue);
	}
}

void UDynamicFirstPerson::SetAnimationLookValues(FVector AnimationCharacterLocation, FRotator AnimationCharacterRotation, float IntendedCameraYaw, float IntendedCameraPitch, float YawOffset, float PitchOffset)
{
	// Set Pitch and Yaw max and mins based on interacting object
	// FIX: Maybe don't need middle as persisting variable
	AnimLockedCharacterLocation = AnimationCharacterLocation;
	AnimLockedCharacterRotation = AnimationCharacterRotation;
	AnimYawMax = IntendedCameraYaw + YawOffset;
	AnimYawMin = IntendedCameraYaw - YawOffset;
	AnimPitchMax = IntendedCameraPitch + PitchOffset;
	AnimPitchMin = IntendedCameraPitch - PitchOffset;
	AnimMaxFlipped = false;
	AnimMinFlipped = false;

	if (AnimYawMax > 360 || AnimYawMin < 0)
	{
		if (AnimYawMax > 360)
		{
			AnimYawMax += -360;
			AnimMaxFlipped = true;
		}
		else if (AnimYawMin < 0)
		{
			AnimYawMin += 360;
			AnimMinFlipped = true;
		}
	}
}
