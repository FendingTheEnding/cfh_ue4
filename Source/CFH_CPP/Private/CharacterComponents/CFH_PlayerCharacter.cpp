// Kenneth Kratzer 2020


#include "CFH_PlayerCharacter.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimBlueprint.h"
#include "CFH_AnimInstance.h"
#include "CFH_CharacterActionHandler.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "DynamicFirstPerson.h"
#include "InViewportHandler.h"
#include "InteractiveObjectDialogueSpeaker.h"


// Sets default values for this actor's properties
ACFH_PlayerCharacter::ACFH_PlayerCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	AttachSubComponents();

	// Assigns Animation Blueprint that gives all base motion animations
	static ConstructorHelpers::FObjectFinder<UAnimBlueprint> AnimBPObj(TEXT("AnimBlueprint'/Game/MyContent/Imports/Glycon/Animations/CFH_AnimationBP.CFH_AnimationBP'")); 
	if (AnimBPObj.Succeeded())
	{
		GetMesh()->SetAnimInstanceClass(AnimBPObj.Object->GeneratedClass);
	}
}

void ACFH_PlayerCharacter::AttachSubComponents()
{
	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	InteractiveObjectDialogueSpeaker = CreateDefaultSubobject<UInteractiveObjectDialogueSpeaker>(FName("Interactive Object Dialogue Speaker"));
	InViewportHandler = CreateDefaultSubobject<UInViewportHandler>(FName("In Viewport Handler"));
	DynamicFirstPerson = CreateDefaultSubobject<UDynamicFirstPerson>(FName("Dynamic First Person"));
	CharacterActionHandler = CreateDefaultSubobject<UCFH_CharacterActionHandler>(FName("Character Action Handler"));
}

void ACFH_PlayerCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	AnimInst = Cast<UCFH_AnimInstance>(GetMesh()->GetAnimInstance());
	if (!AnimInst)
	{
		UE_LOG(LogTemp, Error, TEXT("Check AnimInstance gets loaded in CFH_PlayerCharacter"));
	}
}

// Called every frame
void ACFH_PlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//...
}

void ACFH_PlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &ACFH_PlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACFH_PlayerCharacter::MoveRight);

	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	// FIX: this has been changed and should be simplified
	PlayerInputComponent->BindAxis("Turn", this, &ACFH_PlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACFH_PlayerCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &ACFH_PlayerCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACFH_PlayerCharacter::LookUpAtRate);

	UE_LOG(LogTemp, Warning, TEXT("Axis Bound"))
}

void ACFH_PlayerCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// In not in animation process
		if (AnimInst->GetActionAnim() != EActionAnim::Active && !CharacterActionHandler->IsMoveForAnimActive())
		{
			// add movement in that direction
			AddMovementInput(GetActorForwardVector(), Value);
		}
	}
}

void ACFH_PlayerCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// In not in animation process
		if (AnimInst->GetActionAnim() != EActionAnim::Active && !CharacterActionHandler->IsMoveForAnimActive())
		{
			// add movement in that direction
			AddMovementInput(GetActorRightVector(), Value);
		}
	}
}

void ACFH_PlayerCharacter::TurnAtRate(float Rate)
{
	// Current rotation value based on rate input
	AddControllerYawInput(Rate * (BaseTurnRate)*GetWorld()->GetDeltaSeconds());
}

void ACFH_PlayerCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}