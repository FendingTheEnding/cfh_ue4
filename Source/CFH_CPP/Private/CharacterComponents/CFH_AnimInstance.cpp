// Kenneth Kratzer 2020


#include "CFH_AnimInstance.h"
#include "GameFramework/Character.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"


UCFH_AnimInstance::UCFH_AnimInstance()
{

}

void UCFH_AnimInstance::NativeBeginPlay()
{
	SetupInputComponent();
}

void UCFH_AnimInstance::NativeUpdateAnimation(float DeltaTimeX)
{
	// Very Important Line
	Super::NativeUpdateAnimation(DeltaTimeX);

	SetVelocityAndRotationForBlendSpace();
}

void UCFH_AnimInstance::SetVelocityAndRotationForBlendSpace()
{
	CharVel = GetOwningActor()->GetVelocity();
	CharRot = GetOwningActor()->GetActorRotation();
}

void UCFH_AnimInstance::SetupInputComponent()
{
	UInputComponent* InputComponent = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		InputComponent->BindAction("StanceChange", IE_Pressed, this, &UCFH_AnimInstance::ChangeAnimStance);
	}
}

EAnimStance UCFH_AnimInstance::GetAnimStance() const
{
	return AnimStance;
}

void UCFH_AnimInstance::ChangeAnimStance()
{
	if (AnimStance == EAnimStance::Aggressive) 
	{
		AnimStance = EAnimStance::Passive;
	}
	else if (AnimStance == EAnimStance::Passive) 
	{
		AnimStance = EAnimStance::Aggressive;
	}
}

EActionAnim UCFH_AnimInstance::GetActionAnim() const
{
	return ActionAnim;
}

void UCFH_AnimInstance::StartActionAnim(float SequenceDuration)
{
	if (ActionAnim == EActionAnim::Inactive) 
	{
		ActionAnim = EActionAnim::Active;
		UE_LOG(LogTemp, Warning, TEXT("Acting"));
		FTimerHandle AnimThandle;
		GetWorld()->GetTimerManager().SetTimer(AnimThandle, this, &UCFH_AnimInstance::EndActionAnim, SequenceDuration, false);
	}
}

void UCFH_AnimInstance::EndActionAnim()
{
	if (ActionAnim == EActionAnim::Active) 
	{
		ActionAnim = EActionAnim::Inactive;
		UE_LOG(LogTemp, Warning, TEXT("Inactive"));
	}
}

ETurnAnim UCFH_AnimInstance::GetTurnAnim() const
{
	return TurnAnim;
}

void UCFH_AnimInstance::SetTurnAnim(int32 State)
{
	if (State == 0) 
	{
		TurnAnim = ETurnAnim::Left;
	}
	else if (State == 1) 
	{
		TurnAnim = ETurnAnim::Right;
	}
	else 
	{
		TurnAnim = ETurnAnim::Inactive;
	}
}
