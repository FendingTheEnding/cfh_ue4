// Kenneth Kratzer 2020


#include "InViewportHandler.h"
#include "CFH_PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"

#define OUT

// Sets default values for this component's properties
UInViewportHandler::UInViewportHandler()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UInViewportHandler::BeginPlay()
{
	Super::BeginPlay();

	// ...
	GetAndCheckRequiredClassPointers();
}

bool UInViewportHandler::GetAndCheckRequiredClassPointers()
{
	MyController = Cast<ACFH_PlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	if (!MyController)
	{
		UE_LOG(LogTemp, Error, TEXT("Check GameInstance gets loaded in InViewportHandler"));
		return false;
	}
	return true;
}


// Called every frame
void UInViewportHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

// FIX: Not in use; Do I need
bool UInViewportHandler::ViewDistance(FVector ViewpointLocation, TArray<float> ObjectLocationRotation)
{
	float PlayerObjectDistance = FVector::Dist({ ViewpointLocation.X, ViewpointLocation.Y, 0 }, { ObjectLocationRotation[0], ObjectLocationRotation[1], 0 });

	if (PlayerObjectDistance < ObjectLocationRotation[6])
	{
		return true;
	}
	return false;
}

bool UInViewportHandler::ViewWithinDistance(FVector ObjectLocation, float InteractDistance)
{
	FVector PlayerViewPointLocation;
	FRotator PlayerViewPointRotation;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT PlayerViewPointLocation, OUT PlayerViewPointRotation);
	
	float PlayerObjectDistance = FVector::Dist({ PlayerViewPointLocation.X, PlayerViewPointLocation.Y, 0 }, { ObjectLocation.X, ObjectLocation.Y, 0 });

	return (PlayerObjectDistance < InteractDistance);
}

bool UInViewportHandler::ObjectInViewport(FVector ObjectLocation)
{
	TArray<float> TraceViewAngles;

	TraceViewAngles = GetCameraTraceAngles(ObjectLocation);

	return (TraceViewAngles[0] > 0.f && TraceViewAngles[1] < 0.f && TraceViewAngles[2] < 0.f && TraceViewAngles[3] > 0.f);
}

// FIX: this was nullptr error
TArray<float> UInViewportHandler::GetCameraTraceAngles(FVector ObjectLocation)
{
	FVector CameraTraceLocation;
	FRotator CameraTraceRotation;
	TArray<float> TraceViewAngles;
	for (int i = 0; i < 5; i++)
	{
		// UE_LOG(LogTemp, Warning, TEXT("AttemptingHitTrace"))
		MyController->GetSightRayHitLocation(i, CameraTraceLocation, CameraTraceRotation);
		if (i < 2)
		{
			TraceViewAngles.Add(CameraTraceYawAngle(CameraTraceLocation, CameraTraceRotation, ObjectLocation));
		}
		else
		{
			TraceViewAngles.Add(CameraTracePitchAngle(CameraTraceLocation, CameraTraceRotation, ObjectLocation));
		}
	}
	return TraceViewAngles;
}

// Get yaw angles of viewport sides from object center
float UInViewportHandler::CameraTraceYawAngle(FVector CameraTraceLocation, FRotator CameraTraceRotation, FVector ObjectLocation)
{
	FVector TraceXY = { cosf(CameraTraceRotation.Yaw * (PI / 180)), sinf(CameraTraceRotation.Yaw * (PI / 180)), 0 };
	FVector ObjTraceXY = { ObjectLocation.X - CameraTraceLocation.X, ObjectLocation.Y - CameraTraceLocation.Y, 0 };

	float TraceObjRot = atan2f((TraceXY[0] * ObjTraceXY[1] - ObjTraceXY[0] * TraceXY[1]), FVector::DotProduct(ObjTraceXY, TraceXY));

	return TraceObjRot * (180 / PI);
}

// Get pitch angles of viewport top and bottom from object center
float UInViewportHandler::CameraTracePitchAngle(FVector CameraTraceLocation, FRotator CameraTraceRotation, FVector ObjectLocation)
{
	FVector TraceXY = { cosf(CameraTraceRotation.Pitch * (PI / 180)), sinf(CameraTraceRotation.Pitch * (PI / 180)), 0 };
	FVector ObjTraceXY = { sqrtf(FMath::Square(ObjectLocation.X - CameraTraceLocation.X) + FMath::Square(ObjectLocation.X - CameraTraceLocation.Y)), ObjectLocation.X - CameraTraceLocation.Z, 0 };

	float TraceObjRot = atan2f((TraceXY[0] * ObjTraceXY[1] - ObjTraceXY[0] * TraceXY[1]), FVector::DotProduct(ObjTraceXY, TraceXY));

	return TraceObjRot * (180 / PI);
}

