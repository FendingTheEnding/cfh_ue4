// Kenneth Kratzer 2020


#include "CFH_CharacterActionHandler.h"
#include "ActionObjectModel.h"
#include "CFH_AnimInstance.h"
#include "DynamicFirstPerson.h"
#include "GameFramework/Character.h"
#include "InteractiveObjectDialogueSpeaker.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameInstanceCpp.h"

// Sets default values for this component's properties
UCFH_CharacterActionHandler::UCFH_CharacterActionHandler()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCFH_CharacterActionHandler::BeginPlay()
{
	Super::BeginPlay();

	// ...
	// Assign required objects and check none are nullptred
	if (GetAndCheckRequiredClasses())
	{
		SetupInputComponent();
	}

}

bool UCFH_CharacterActionHandler::GetAndCheckRequiredClasses()
{
	MyCharacter = Cast<ACharacter>(GetOwner());
	if (MyCharacter && ActionAnim)
	{
		AnimInstance = Cast<UCFH_AnimInstance>(MyCharacter->GetMesh()->GetAnimInstance());
		CharacterSpeaker = Cast<UInteractiveObjectDialogueSpeaker>(MyCharacter->FindComponentByClass<UInteractiveObjectDialogueSpeaker>());
		DynamicFirstPerson = Cast<UDynamicFirstPerson>(MyCharacter->FindComponentByClass<UDynamicFirstPerson>());
		if (!AnimInstance || !CharacterSpeaker || !DynamicFirstPerson)
		{
			UE_LOG(LogTemp, Error, TEXT("CharacterActionHandler failure to load DynamciFirstPerson, AnimInstance or CharacterSpeaker from MyCharacter"))
			return false;
		}
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("CharacterActionHandler failure to load MyCharacter or ActionAnim; ActionAnim has to be loaded in blueprint."))
		return false;
	}

	return true;
}

void UCFH_CharacterActionHandler::SetupInputComponent()
{
	// Get from Player's Character
	UInputComponent* InputComponent = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0))->FindComponentByClass<UInputComponent>();
	if (InputComponent)
	{
		// Bind actions
		InputComponent->BindAction("AudioInteract", IE_Pressed, this, &UCFH_CharacterActionHandler::PassToAudioInput);
		InputComponent->BindAction("PhysicalInteract", IE_Pressed, this, &UCFH_CharacterActionHandler::CallAnimAction);
	}
}

AActionObjectModel* UCFH_CharacterActionHandler::ActiveActionObject()
{
	return ReferenceActionObjectModel->ActiveActionObject;
}

// Called every frame
void UCFH_CharacterActionHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (bMoveForAnim)
	{
		MoveToAnimLocation();	
	}
}

void UCFH_CharacterActionHandler::MoveToAnimLocation()
{
	MyCharacter->Controller->SetControlRotation(FMath::RInterpTo(MyCharacter->Controller->GetControlRotation(), ActiveActionObject()->CharacterRotation, GetWorld()->GetDeltaSeconds(), 3.f));
	MyCharacter->SetActorLocation(FMath::VInterpTo(MyCharacter->GetActorLocation(), AnimationLocation, GetWorld()->GetDeltaSeconds(), 3.f));
	if (abs(MyCharacter->GetActorLocation().Y) > abs(AnimationLocation.Y) - 0.25f && abs(MyCharacter->GetActorLocation().Y) < abs(AnimationLocation.Y) + 0.25f)
	{
		FinishedMoveToAnimLocationActions();
	}
}

void UCFH_CharacterActionHandler::FinishedMoveToAnimLocationActions()
{
	FVector AnimationCharacterLocation = AnimationLocation + MyCharacter->GetMesh()->GetRelativeLocation();
	FRotator AnimationCharacterRotation = MyCharacter->GetMesh()->GetRelativeRotation() + FRotator({ 0, ActiveActionObject()->CharacterRotation.Yaw, 0 });
	DynamicFirstPerson->SetAnimationLookValues(
		AnimationCharacterLocation, AnimationCharacterRotation,
		ActiveActionObject()->CharacterRotation.Yaw,
		ActiveActionObject()->CharacterRotation.Pitch,
		ActiveActionObject()->CharacterRotationOffset.Yaw,
		ActiveActionObject()->CharacterRotationOffset.Pitch
	);
	bMoveForAnim = false;
	PerformAnimation();
	ActiveActionObject()->PerformOnAnim();
}

bool UCFH_CharacterActionHandler::ProceedWithAction(FString ActionType)
{
	if (ActionType == FString("Animation"))
	{
		if (AnimInstance->GetAnimStance() == EAnimStance::Aggressive)
		{
			UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction(%s): Aggressive"), *ActionType)
			return ActiveActionObject()->PlayerActionRequest(EActionType::AggressiveAnim);
		}
		else if (AnimInstance->GetAnimStance() == EAnimStance::Passive)
		{
			UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction(%s): Passive"), *ActionType)
			return ActiveActionObject()->PlayerActionRequest(EActionType::PassiveAnim);
		}
	}
	else if (ActionType == FString("Audio"))
	{
		if (AnimInstance->GetAnimStance() == EAnimStance::Aggressive)
		{
			UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction(%s): Aggressive"), *ActionType)
			return ActiveActionObject()->PlayerActionRequest(EActionType::PlayerAggroSpeak);
		}
		else if (AnimInstance->GetAnimStance() == EAnimStance::Passive)
		{
			UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction(%s): Passive"), *ActionType)
			return ActiveActionObject()->PlayerActionRequest(EActionType::PlayerPassSpeak);
		}
	}
	else if (ActionType == FString("Object"))
	{
		if (AnimInstance->GetAnimStance() == EAnimStance::Aggressive)
		{
			UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction(%s): Aggressive"), *ActionType)
			return ActiveActionObject()->ObjectActionRequest(EActionType::NPCAggroSpeak);
		}
		else if (AnimInstance->GetAnimStance() == EAnimStance::Passive)
		{
			UE_LOG(LogTemp, Warning, TEXT("ProceedWithAction(%s): Passive"), *ActionType)
			return ActiveActionObject()->ObjectActionRequest(EActionType::NPCPassSpeak);
		}
	}
	return false;
}

void UCFH_CharacterActionHandler::CallAnimAction()
{
	if (ActiveActionObject()->bObjectInAction)
	{
		if (ProceedWithAction(FString("Animation")))
		{
			AnimationLocation = ActiveActionObject()->GetAnimationPositioning();
			bMoveForAnim = true;
		}
	}
}

void UCFH_CharacterActionHandler::PerformAnimation()
{
	// Active Animation is assigned when ProceedWithAction is returned true
	UAnimSequence* AnimationSequence = ActiveActionObject()->ActiveActionSequence->AnimationReference;

	if (AnimationSequence) {
		float SeqDuration = (AnimationSequence->GetRawNumberOfFrames() / AnimationSequence->GetFrameRate());

		// Set Animation to be called
		ActionAnim->CreateAnimation(AnimationSequence);

		AnimInstance->StartActionAnim(SeqDuration);
		UE_LOG(LogTemp, Warning, TEXT("ActionCalled"));
	}
}

void UCFH_CharacterActionHandler::PassToAudioInput()
{
	// False to say its a PlayerRequest and not an NPCRequest
	CallAudioInput(false);
}

void UCFH_CharacterActionHandler::CallAudioInput(bool ObjectRequest)
{
	if (ActiveActionObject()->bObjectInAction)
	{
		if (!ObjectRequest)
		{
			if (ProceedWithAction(FString("Audio")))
			{
				PlayActiveAudio();
			}
		}
		else if (ProceedWithAction(FString("Object")))
		{
			PlayActiveAudio();
		}
	}
}

void UCFH_CharacterActionHandler::PlayActiveAudio()
{
	CharacterSpeaker->PlayObjectAudio(ActiveActionObject()->ActiveActionSequence->AudioSubtitles, ActiveActionObject()->ActiveActionSequence->AudioReference, false);
}

void UCFH_CharacterActionHandler::AssignReferenceActionObjectModel(AActionObjectModel* ActionObjectModel)
{
	ReferenceActionObjectModel = ActionObjectModel;
}

bool UCFH_CharacterActionHandler::IsMoveForAnimActive()
{
	return bMoveForAnim;
}
