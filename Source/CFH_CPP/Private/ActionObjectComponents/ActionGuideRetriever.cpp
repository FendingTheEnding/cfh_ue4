// Kenneth Kratzer 2020


#include "ActionGuideRetriever.h"
#include "ActionObjectModel.h"
#include "MyGameInstanceCpp.h"

// Sets default values for this component's properties
UActionGuideRetriever::UActionGuideRetriever()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UActionGuideRetriever::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UActionGuideRetriever::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

FActionGuide* UActionGuideRetriever::GetActionGuide(FName ActionObjectName, UMyGameInstanceCpp* GI)
{
	FActionGuide* ActionGuide = GI->GetActionObjectSequence(ActionObjectName);
	if (!ActionGuide)
	{
		UE_LOG(LogTemp, Error, TEXT("%s: Action Guide not loaded!!"), *ActionObjectName.ToString());
		return nullptr;
	}
	else
	{
		return ActionGuide;
	}
}