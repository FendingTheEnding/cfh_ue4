// Kenneth Kratzer 2020


#include "ActionObjectVisualEffectsHandler.h"
#include "ActionObjectModel.h"
#include "Runtime/Engine/Classes/Kismet/KismetMaterialLibrary.h"
#include "Materials/MaterialParameterCollection.h"

// Sets default values for this component's properties
UActionObjectVisualEffectsHandler::UActionObjectVisualEffectsHandler()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	// Gets DataTable containting reference information for all interactable objects (Radius and Start Position)
	static ConstructorHelpers::FObjectFinder<UMaterialParameterCollection> ScanMPCObject(TEXT("MaterialParameterCollection'/Game/MyContent/Materials/PC_ScanRadius.PC_ScanRadius'"));
	if (ScanMPCObject.Succeeded())
	{
		ScanMPC = ScanMPCObject.Object;
	}
}


// Called when the game starts
void UActionObjectVisualEffectsHandler::BeginPlay()
{
	Super::BeginPlay();

	// ...
	OwningActionObjectModel = Cast<AActionObjectModel>(GetOwner());
}


// Called every frame
void UActionObjectVisualEffectsHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...

	if (bPerformObjectScan)
	{
		InterpolatedObjectScan();
	}
}

void UActionObjectVisualEffectsHandler::InterpolatedObjectScan()
{
	float CurrentRadius = UKismetMaterialLibrary::GetScalarParameterValue(GetWorld(), ScanMPC, FName("Radius"));
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), ScanMPC, FName("Radius"), FMath::FInterpTo(CurrentRadius, 1, GetWorld()->GetDeltaSeconds(), ScanRate));
}

void UActionObjectVisualEffectsHandler::ScanObject()
{
	SetScanMPCValues(OwningActionObjectModel->GetActorLocation());
	ActivateObjectScan();
	SetDeactivateObjectScanTimer();
}

void UActionObjectVisualEffectsHandler::ActivateObjectScan()
{
	OwningActionObjectModel->SetRenderDepth(true);
	bPerformObjectScan = true;
}

void UActionObjectVisualEffectsHandler::SetDeactivateObjectScanTimer()
{
	FTimerHandle ScanThandle;
	GetWorld()->GetTimerManager().SetTimer(ScanThandle, this, &UActionObjectVisualEffectsHandler::DeactivateObjectScan, ScanTime, false);
}

void UActionObjectVisualEffectsHandler::DeactivateObjectScan()
{
	bPerformObjectScan = false;
	OwningActionObjectModel->SetRenderDepth(false);
}

void UActionObjectVisualEffectsHandler::SetScanMPCValues(FVector ObjectLocation)
{
	UKismetMaterialLibrary::SetScalarParameterValue(GetWorld(), ScanMPC, FName("Radius"), 0);
	UKismetMaterialLibrary::SetVectorParameterValue(GetWorld(), ScanMPC, FName("ObjectLocation"), ObjectLocation);
}

void UActionObjectVisualEffectsHandler::SetWidgetVisibility(bool bValue)
{
	if (bValue)
	{
		VisibilityState = EWidgetVisibility::Seen;
	}
	else
	{
		VisibilityState = EWidgetVisibility::Unseen;
	}
}

EWidgetVisibility UActionObjectVisualEffectsHandler::GetVisibilityState() const
{
	return VisibilityState;
}