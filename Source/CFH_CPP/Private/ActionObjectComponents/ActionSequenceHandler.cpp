// Kenneth Kratzer 2020


#include "ActionSequenceHandler.h"

// Sets default values for this component's properties
UActionSequenceHandler::UActionSequenceHandler()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SequenceHandlerReferenceDT = GetSequenceHandlerReferenceDT();
}

UDataTable* UActionSequenceHandler::GetSequenceHandlerReferenceDT()
{
	// Gets DataTable containting reference information for all interactable objects
	static ConstructorHelpers::FObjectFinder<UDataTable> SequenceHandlerReferenceDTObject(TEXT("DataTable'/Game/MyContent/DataTables/SequenceHandlerReferenceDT.SequenceHandlerReferenceDT'"));
	if (SequenceHandlerReferenceDTObject.Succeeded())
	{
		return SequenceHandlerReferenceDTObject.Object;
	}
	UE_LOG(LogTemp, Error, TEXT("SequenceHandlerReferenceDT not assigned: ActionSequenceHandler.ccp (16)"))
	return nullptr;
}

// Called when the game starts
void UActionSequenceHandler::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UActionSequenceHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UActionSequenceHandler::SequenceHandlerSetup(FActionGuide* ActionGuide)
{
	SetActionGuideVariables(ActionGuide);
	PopulateActionSequenceIndexesArray();
	AddThisToSequenceHandlerReferenceDT();

	SetNextSequenceOptions();
}

void UActionSequenceHandler::SetActionGuideVariables(FActionGuide* ActionGuide)
{
	FString ObjectReferenceNumber = ActionGuide->ReferenceNumber;
	HexIdentifier = FParse::HexNumber(*ObjectReferenceNumber);

	ActionSequences = ActionGuide->SequenceInformation;
}

void UActionSequenceHandler::PopulateActionSequenceIndexesArray()
{
	for (int32 i = 0; i < (int32)EActionType::EndStage + 1; i++)
	{
		ActionMarkers.ActionSequenceIndexes.Add(0);
	}
}

void UActionSequenceHandler::AddThisToSequenceHandlerReferenceDT()
{
	FSequenceHandlerReferences ThisToAddToDT;
	ThisToAddToDT.HandlerReferenceArray = this;
	FName HexIdentifiesAsFName = FName(FString::FromInt(HexIdentifier));
	SequenceHandlerReferenceDT->AddRow(HexIdentifiesAsFName, ThisToAddToDT);
}

void UActionSequenceHandler::SetNextSequenceOptions()
{
	ResetValuesForNextSequence();

	int32 ActiveSequenceIndex = ActionMarkers.ActiveSequenceIndex;
	TArray<FString> NextSequenceOptions = ActionSequences[ActiveSequenceIndex].NextSequenceOptions;
	for (int32 i = 0; i < NextSequenceOptions.Num(); i++)
	{
		SetActionTypeMarker(NextSequenceOptions[i]);
	}
}

void UActionSequenceHandler::ResetValuesForNextSequence()
{
	RetargetAssignment = false;

	FActionSequence ResetActiveSequence;
	ActiveActionSequence = ResetActiveSequence;

	ResetActionMarkers();
}

void UActionSequenceHandler::ResetActionMarkers()
{
	for (int32 i = 0; i < ActionMarkers.ActionSequenceIndexes.Num(); i++)
	{
		ActionMarkers.ActionSequenceIndexes[i] = 0;
	}
}

void UActionSequenceHandler::SetActionTypeMarker(FString NextSequenceOption)
{
	int32 HexRequest = FParse::HexNumber(*NextSequenceOption.Left(3));
	int32 SequenceIndex = FCString::Atoi(*NextSequenceOption.RightChop(3));
	if (HexRequest == HexIdentifier)
	{
		AssignMarkerForThis(SequenceIndex);
	}
	else
	{
		AssignMarkerForExternal(HexRequest, NextSequenceOption);
	}
}

void UActionSequenceHandler::AssignMarkerForThis(int32 SequenceIndex)
{
	EActionType PossibleNextActionType = ActionSequences[SequenceIndex].SequenceType;
	// UE_LOG(LogTemp, Warning, TEXT("SettingPossibleActions: %d, %d"), SequenceIndex, (int32)PossibleNextActionType)
	ActionMarkers.ActionSequenceIndexes[(int32)PossibleNextActionType] = SequenceIndex;
	// UE_LOG(LogTemp, Warning, TEXT("SequencesAssigned: %d, %d, %d"), (int32)PossibleNextActionType, ActionMarkers.ActionSequenceIndexes[(int32)PossibleNextActionType], SequenceIndex)
}

void UActionSequenceHandler::AssignMarkerForExternal(int32 HexRequest, FString NextSequenceOption)
{
	FString ContextString;
	FSequenceHandlerReferences* RetargetActionGuide = SequenceHandlerReferenceDT->FindRow<FSequenceHandlerReferences>(FName(FString::FromInt(HexRequest)), ContextString);
	if (RetargetActionGuide)
	{
		RetargetActionGuide->HandlerReferenceArray->RetargetSequenceAssignment(NextSequenceOption);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Object %d: RetargetActionGuide is nullptr. Check that NextSequenceOptions for object are valid"), HexIdentifier);
	}
}

void UActionSequenceHandler::RetargetSequenceAssignment(FString RetargetSequence)
{
	// Reset variables so that retarget completely redoes NextSequence setup
	if (!RetargetAssignment)
	{
		RetargetAssignment = true;
		ResetActionMarkers();
	}
	SetActionTypeMarker(RetargetSequence);
}

void UActionSequenceHandler::SetActiveSequenceValues(float ActionBufferTime)
{
	if (!bSequenceActive)
	{
		// UE_LOG(LogTemp, Warning, TEXT("SetActiveSequenceValues Called: %d"), (int32)ActiveActionSequence.SequenceType);

		bSequenceActive = true;
		int32 ActiveSequenceIndex = ActionMarkers.ActiveSequenceIndex;
		ActiveActionSequence = ActionSequences[ActiveSequenceIndex];

		SetValuesForSequenceActionEnd(ActionBufferTime);
	}
}

void UActionSequenceHandler::SetValuesForSequenceActionEnd(float ActionBufferTime)
{
	if (IsPlayerSequenceType() || IsNPCSequenceType())
	{
		SetSequenceDuration();

		PrepSequenceActive(ActionBufferTime);
	}
	else if (IsAnimSequenceType())
	{
		SetSequenceDuration();

		// PrepSequenceActive called after character moved into proper position
	}
}

bool UActionSequenceHandler::IsPlayerSequenceType()
{
	return (ActiveActionSequence.SequenceType == EActionType::PlayerSpeak ||
		ActiveActionSequence.SequenceType == EActionType::PlayerAggroSpeak ||
		ActiveActionSequence.SequenceType == EActionType::PlayerPassSpeak);
}

bool UActionSequenceHandler::IsNPCSequenceType()
{
	return (ActiveActionSequence.SequenceType == EActionType::NPCSpeak ||
		ActiveActionSequence.SequenceType == EActionType::NPCAggroSpeak ||
		ActiveActionSequence.SequenceType == EActionType::NPCPassSpeak);
}

bool UActionSequenceHandler::IsAnimSequenceType()
{
	return (ActiveActionSequence.SequenceType == EActionType::AggressiveAnim || 
		ActiveActionSequence.SequenceType == EActionType::PassiveAnim);
}

void UActionSequenceHandler::SetSequenceDuration()
{
	if (IsAnimSequenceType())
	{
		SequenceDuration = (ActiveActionSequence.AnimationReference->GetRawNumberOfFrames() / ActiveActionSequence.AnimationReference->GetFrameRate());
	}
	else
	{
		SequenceDuration = ActiveActionSequence.AudioReference->GetDuration();
	}
}

void UActionSequenceHandler::PrepSequenceActive(float ActionBufferTime)
{
	// Set timer based on time action takes to process
	// Fix: Add possible delay time additions
	FTimerHandle SeqThandle;
	GetWorld()->GetTimerManager().SetTimer(SeqThandle, this, &UActionSequenceHandler::SetSequenceActive, SequenceDuration + ActionBufferTime, false);
	UE_LOG(LogTemp, Warning, TEXT("Timer Set: %f"), SequenceDuration + ActionBufferTime);
}

void UActionSequenceHandler::SetSequenceActive()
{
	UE_LOG(LogTemp, Warning, TEXT("SettingNextSequenceOptoins"))
	
	bSequenceActive = false;
	SetNextSequenceOptions();
}

bool UActionSequenceHandler::PlayerActionRequest(EActionType RequestedAction, bool ObjectInViewport)
{
	if (!bSequenceActive)
	{
		if (IsRequestedActionValid(RequestedAction))
		{
			if (IsAnimAction(RequestedAction))
			{
				if (ObjectInViewport)
				{
					AssignActiveSequenceIndex(RequestedAction);
					return true;
				}
			}
			else
			{
				AssignActiveSequenceIndex(RequestedAction);
				return true;
			}
		}
		return CheckDefaultSpeakTypes();
	}
	return false;
}

bool UActionSequenceHandler::ObjectActionRequest(EActionType RequestedAction)
{
	if (!bSequenceActive)
	{
		if (IsRequestedActionValid(RequestedAction))
		{
			AssignActiveSequenceIndex(RequestedAction);
			return true;
		}
		return CheckDefaultSpeakTypes();
	}
	return false;
}

bool UActionSequenceHandler::CheckDefaultSpeakTypes()
{
	if (IsRequestedActionValid(EActionType::PlayerSpeak))
	{
		AssignActiveSequenceIndex(EActionType::PlayerSpeak);
		return true;
	}
	else if (IsRequestedActionValid(EActionType::NPCSpeak))
	{
		AssignActiveSequenceIndex(EActionType::NPCSpeak);
		return true;
	}
	return false;
}

void UActionSequenceHandler::AssignActiveSequenceIndex(EActionType RequestedAction)
{
	ActionMarkers.ActiveSequenceIndex = ActionMarkers.ActionSequenceIndexes[(int32)RequestedAction];
}

bool UActionSequenceHandler::IsRequestedActionValid(EActionType RequestedAction)
{
	return (bool(ActionMarkers.ActionSequenceIndexes[(int32)RequestedAction]));
}

bool UActionSequenceHandler::IsAnimAction(EActionType RequestedAction)
{
	return (RequestedAction == EActionType::AggressiveAnim || RequestedAction == EActionType::PassiveAnim);
}

FActionSequenceHandlerMarkers* UActionSequenceHandler::GetActionSequenceHandlerMarkersPointer()
{
	FActionSequenceHandlerMarkers* ActionSequenceHandlerMarkersPointer = &ActionMarkers;

	return ActionSequenceHandlerMarkersPointer;
}

FActionSequence* UActionSequenceHandler::GetActiveActionSequencePointer()
{
	FActionSequence* ActiveActionSequencePointer = &ActiveActionSequence;

	return ActiveActionSequencePointer;
}