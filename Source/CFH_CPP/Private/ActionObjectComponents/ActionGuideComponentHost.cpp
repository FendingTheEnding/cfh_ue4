// Kenneth Kratzer 2020


#include "ActionGuideComponentHost.h"
#include "ActionGuideRetriever.h"

#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UActionGuideComponentHost::UActionGuideComponentHost()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Add all action guide components
	ActionGuideRetriever = CreateDefaultSubobject<UActionGuideRetriever>(FName("Action Guide Retriever"));
	ActionSequenceHandler = CreateDefaultSubobject<UActionSequenceHandler>(FName("Action Sequence Handler"));
}


// Called when the game starts
void UActionGuideComponentHost::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UActionGuideComponentHost::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UActionGuideComponentHost::HostSetup(FName ActionObjectName)
{
	FActionGuide* ActionGuide = RetrieveActionGuide(ActionObjectName);
	if (ActionGuide)
	{
		ActionSequenceHandler->SequenceHandlerSetup(ActionGuide);
		UE_LOG(LogTemp, Warning, TEXT("Object Initialized: %s"), *ActionObjectName.ToString());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("%s: Action Guide not loaded or InValid!!"), *ActionObjectName.ToString());
	}
}

FActionGuide* UActionGuideComponentHost::RetrieveActionGuide(FName ActionObjectName)
{
	UMyGameInstanceCpp* GI = Cast<UMyGameInstanceCpp>(GetWorld()->GetGameInstance());
	return ActionGuideRetriever->GetActionGuide(ActionObjectName, GI);
}

UActionSequenceHandler* UActionGuideComponentHost::GetActionSequenceHandler()
{
	return ActionSequenceHandler;
}