// Kenneth Kratzer 2020


#include "StandardActionObject.h"
#include "ActionObjectVisualEffectsHandler.h"
#include "CFH_CharacterActionHandler.h"
#include "InViewportHandler.h"

AStandardActionObject::AStandardActionObject()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//AStandardActionObject::ObjectInAction = 0;

	ActionObjectVisualEffectsHandler = CreateDefaultSubobject<UActionObjectVisualEffectsHandler>(FName("Action Object Visual Effects Handler"));
}

// Called when the game starts or when spawned
void AStandardActionObject::BeginPlay()
{
	Super::BeginPlay();

	GetVisualEffectsHandler(ActionObjectVisualEffectsHandler);

}

// Called every frame
void AStandardActionObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!bEndStage)
	{
		if (!bThisInAction)
		{
			if (IsObjectInSpeakDistance())
			{
				if (InViewportHandler->ObjectInViewport(GetActorLocation()))
				{
					// Set all actives for regular functions
					if (!bObjectInAction)
					{
						ActivateObject();
					}
				}
			}
		}
		else
		{
			// Check if Action is currently being performed
			if (!ActionSequenceHandler->bSequenceActive)
			{
				if (IsActionNPC())
				{
					CharacterActionHandler->CallAudioInput(true);
				}
				// Set if EndStage
				else if (IsActionEndStage())
				{
					DeactivateObject();
					bEndStage = true;
					UE_LOG(LogTemp, Error, TEXT("%s: EndStage"), *ActionGuideName.ToString());
				}
				else if (IsObjectInSpeakDistance() && InViewportHandler->ObjectInViewport(GetActorLocation()))
				{
					SetDeactivationTime();
				}
				else if (!IsObjectInSpeakDistance() && IsDeactivationTime())
				{
					DeactivateObject();
				}
				bFirstTickOfActiveSequence = true;
			}
			else
			{
				if (bFirstTickOfActiveSequence)
				{
					bFirstTickOfActiveSequence = false;
					CheckActiveSequenceOptions();
				}
				SetDeactivationTime();
			}
		}
	}
}

void AStandardActionObject::ActivateObject()
{
	bThisInAction = true;
	bObjectInAction = true;
	ActiveActionObject = this;
	UE_LOG(LogTemp, Warning, TEXT("Object Set Active"))
}

void AStandardActionObject::DeactivateObject()
{
	bThisInAction = false;
	bObjectInAction = false;
	ActionObjectVisualEffectsHandler->SetWidgetVisibility(false);
	UE_LOG(LogTemp, Error, TEXT("%s: Inactive"), *ActionGuideName.ToString());
}

void AStandardActionObject::CheckActiveSequenceOptions()
{
	if (ActiveActionSequence->ScanObjectOnAction)
	{
		ActionObjectVisualEffectsHandler->ScanObject();
		UE_LOG(LogTemp, Error, TEXT("Scanning"))
	}
	// Sets object marker on if off and off if on
	if (ActiveActionSequence->ToggleObjectMarker)
	{
		ToggleVisibility();
	}
}

void AStandardActionObject::SetDeactivationTime()
{
	DeactivationTime = GetWorld()->TimeSeconds + InactiveBufferTime;
}

bool AStandardActionObject::IsDeactivationTime()
{
	return GetWorld()->TimeSeconds > DeactivationTime;
}

void AStandardActionObject::ToggleVisibility()
{
	//UE_LOG(LogTemp, Warning, TEXT("Object being Marked: %s"), ((bool)ActionObjectVisualEffectsHandler->GetVisibilityState() ? TEXT("true") : TEXT("false")))
	if ((bool)ActionObjectVisualEffectsHandler->GetVisibilityState())
	{
		ActionObjectVisualEffectsHandler->SetWidgetVisibility(false);
	}
	else
	{
		//UE_LOG(LogTemp, Warning, TEXT("Object being Marked: Set True"))
		ActionObjectVisualEffectsHandler->SetWidgetVisibility(true);
	}
}

bool AStandardActionObject::IsActionNPC()
{
	return ((bool)ActionMarkers->ActionSequenceIndexes[(int32)EActionType::NPCAggroSpeak] ||
		(bool)ActionMarkers->ActionSequenceIndexes[(int32)EActionType::NPCPassSpeak] ||
		(bool)ActionMarkers->ActionSequenceIndexes[(int32)EActionType::NPCSpeak]);
}

bool AStandardActionObject::IsActionEndStage()
{
	return (bool)ActionMarkers->ActionSequenceIndexes[(int32)EActionType::EndStage];
}

bool AStandardActionObject::IsObjectInSpeakDistance()
{
	return InViewportHandler->ViewWithinDistance(GetActorLocation(), SpeakDistance);
}