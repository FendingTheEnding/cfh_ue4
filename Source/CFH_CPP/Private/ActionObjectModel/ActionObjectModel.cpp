// Kenneth Kratzer 2020


#include "ActionObjectModel.h"
#include "ActionGuideComponentHost.h"
#include "ActionGuideRetriever.h"
#include "CFH_CharacterActionHandler.h"
#include "InViewportHandler.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "MyGameInstanceCpp.h"

// Sets default values
AActionObjectModel::AActionObjectModel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Used as root component so meshes can be moved within blueprint
	SetupSceneRootComponent();

	// Add all action guide components
	ActionGuideComponentHost = CreateDefaultSubobject<UActionGuideComponentHost>(FName("Action Guide Component Host"));

}

void AActionObjectModel::SetupSceneRootComponent()
{
	SceneRoot = CreateDefaultSubobject<USceneComponent>(FName("SceneRoot"));
	SetRootComponent(SceneRoot);
	SceneRoot->SetVisibility(false);
	SceneRoot->SetMobility(EComponentMobility::Static);
}

// Called when the game starts or when spawned
void AActionObjectModel::BeginPlay()
{
	Super::BeginPlay();
	
	if (GetAndCheckRequiredClassPointers())
	{
		ActionGuideComponentHost->HostSetup(ActionGuideName);
		ActionMarkers = ActionSequenceHandler->GetActionSequenceHandlerMarkersPointer();
		ActiveActionSequence = ActionSequenceHandler->GetActiveActionSequencePointer();

		bEndStage = false;
		UE_LOG(LogTemp, Warning, TEXT("Object Initialized: %s"), *ActionGuideName.ToString());
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Check GameInstance MyCharacter InViewportHandler and InteractiveObjectDialogueSpeaker get loaded"));
	}
}

bool AActionObjectModel::GetAndCheckRequiredClassPointers()
{
	MyCharacter = Cast<ACharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
	InViewportHandler = MyCharacter->FindComponentByClass<UInViewportHandler>();
	CharacterActionHandler = MyCharacter->FindComponentByClass<UCFH_CharacterActionHandler>();
	ActionSequenceHandler = ActionGuideComponentHost->GetActionSequenceHandler();

	return (MyCharacter && InViewportHandler && CharacterActionHandler && ActionSequenceHandler);
}

void AActionObjectModel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// ...
}

FVector AActionObjectModel::GetAnimationPositioning()
{
	FVector MoveEndLocation = { 0, 0, 0 };
	FVector ObjectLocation = GetActorLocation();
	FVector PlayerLocation = MyCharacter->GetActorLocation();
	for (int32 i = 0; i < 3; i++)
	{
		// Direction in line with object
		if (CharacterPositioning[i] == 1)
		{
			MoveEndLocation[i] = ObjectLocation[i];
		}
		// Keep Z direction so height doesn't change
		else if (CharacterPositioning[i] == 0)
		{
			MoveEndLocation[i] = PlayerLocation[i];
		}
		// Set distance character is from object
		else
		{
			MoveEndLocation[i] = CharacterPositioning[i];
		}
	}

	return MoveEndLocation;
}

bool AActionObjectModel::PlayerActionRequest(EActionType RequestedAction)
{
	if (ActionSequenceHandler->PlayerActionRequest(RequestedAction, InViewportHandler->ObjectInViewport(GetActorLocation())))
	{
		PreActionPrep();
		return true;
	}
	return false;
}

bool AActionObjectModel::ObjectActionRequest(EActionType RequestedAction)
{
	if (ActionSequenceHandler->ObjectActionRequest(RequestedAction))
	{
		PreActionPrep();
		return true;
	}
	return false;
}

void AActionObjectModel::PerformOnAnim()
{
	ActionSequenceHandler->PrepSequenceActive(ActionBufferTime);
}

void AActionObjectModel::PreActionPrep()
{
	ActionSequenceHandler->SetActiveSequenceValues(ActionBufferTime);
}

void AActionObjectModel::SetRenderDepth(bool bValue) const
{
	// Used by sub-classes to set render depth 
	// Passed to sub-class so they can decide if used on static mesh, or skeletal mesh, or etc.
}