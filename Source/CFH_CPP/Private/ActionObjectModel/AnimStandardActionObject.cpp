// Kenneth Kratzer 2020


#include "AnimStandardActionObject.h"

AAnimStandardActionObject::AAnimStandardActionObject()
{
	// Set main objects mesh component
	SkeletalMeshObject = CreateDefaultSubobject<USkeletalMeshComponent>(FName("Skeletal Mesh Object"));
	SkeletalMeshObject->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

void AAnimStandardActionObject::BeginPlay()
{
	// Make sure name is set for ActionGuide retrieval before attempting to retrieve
	if (ActionGuideName == FName(""))
	{
		UE_LOG(LogTemp, Error, TEXT("AnimStandardActionObject has blank ActionGuideName Value"));
	}
	else
	{
		Super::BeginPlay();
	}
}

void AAnimStandardActionObject::PerformOnAnim()
{
	// Check ObjectAnimation is not null and then call action to perform
	if (ActiveActionSequence->ObjectAnimationReference)
	{
		SkeletalMeshObject->PlayAnimation(ActiveActionSequence->ObjectAnimationReference, false);
	}
	Super::PerformOnAnim();
}

void AAnimStandardActionObject::SetRenderDepth(bool bValue) const
{
	SkeletalMeshObject->SetRenderCustomDepth(bValue);
}