// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CFH_CharacterActionHandler.generated.h"

// Forward Declarations
class AActionObjectModel;
class UMyGameInstanceCpp;
class UAnimSequence;
class UCFH_AnimInstance;
class UDynamicFirstPerson;
class UInteractiveObjectDialogueSpeaker;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UCFH_CharacterActionHandler : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCFH_CharacterActionHandler();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY()
	UDynamicFirstPerson* DynamicFirstPerson{ nullptr };
	// Reference to character where many of these objects are pulled from
	UPROPERTY()
	ACharacter* MyCharacter{ nullptr };
	// Animation Instance used to check stance and set animation to play
	UPROPERTY()
	UCFH_AnimInstance* AnimInstance{ nullptr };
	// Speaker to play action guide audio from
	UPROPERTY()
	UInteractiveObjectDialogueSpeaker* CharacterSpeaker{ nullptr };
	// Animation to be used for object to call new animation from action guide
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	UAnimSequence* ActionAnim;

	// Get and validate DynamicFirstPerson, MyCharacter, and CharacterSpeaker
	bool GetAndCheckRequiredClasses();
	// Used in tick to allow interpolation to AnimationLocation
	bool bMoveForAnim{ false };

	AActionObjectModel* ReferenceActionObjectModel{ nullptr };
	// Used to check if the requested action is an option for the currently active object
	bool ProceedWithAction(FString ActionType);
	// Passes to CallAudioInput, this is done since BindInput requires a function with 0 parameters
	void PassToAudioInput();
	// Used to setup the action inputs to the controller
	void SetupInputComponent();
	// Returns the current ActiveActionObject from ReferenceActionObjectModel
	AActionObjectModel* ActiveActionObject();
	// Gets, Sets, and Calls animation for appropriate components
	void PerformAnimation();
	// Location for character to be during animation
	FVector AnimationLocation{ 0,0,0 };
	// Interpolate character to position for animation
	void MoveToAnimLocation();
	// Set post interpolated move variables
	void FinishedMoveToAnimLocationActions();
	// Play active audio in InteractiveObjectDialogueSpeaker
	void PlayActiveAudio();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Is called on for each 'action' or animation call the player makes
	void CallAnimAction();
	// Is called for every audio call NPC or Player based; All Dialogue goes through this function
	void CallAudioInput(bool ObjectRequest);
	// Assign an ActionObjectModel to gain access to static variables
	void AssignReferenceActionObjectModel(AActionObjectModel* ActionObjectModel);
	// Check if character moving for animation
	bool IsMoveForAnimActive();
};
