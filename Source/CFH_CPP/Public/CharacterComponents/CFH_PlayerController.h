// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "CFH_PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CFH_CPP_API ACFH_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	//Return an OUT parameter, true if hit landscape
	bool GetSightRayHitLocation(int i, FVector& CameraTraceLocation, FRotator& CameraTraceRotation) const;
	// Gets vector of look direction
	bool GetLookDirection(FVector2D ScreenLocation, FVector& CameraTraceLocation, FVector& CameraTraceDirection) const;

private:
	float TraceLocationX[4] = { 0, 1, 0.5, 0.5 };
	float TraceLocationY[4] = { 0.5, 0.5, 0, 1 };
};
