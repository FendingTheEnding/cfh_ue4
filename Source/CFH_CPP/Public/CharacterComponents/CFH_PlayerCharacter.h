// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "CFH_PlayerCharacter.generated.h"

/**
 * Character built on initial UE First Person Character
 */

// Forward Declarations
class UCFH_CharacterActionHandler;
class UDynamicFirstPerson;
class UInViewportHandler;
class UInteractiveObjectDialogueSpeaker;

UCLASS()
class CFH_CPP_API ACFH_PlayerCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Pawn mesh: 1st person view (arms; seen only by self) */
	//UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	//class USkeletalMeshComponent* Mesh1P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;
	/** In Viewport Handler for objects to see if they are in view **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UInViewportHandler* InViewportHandler = nullptr;
	/** Dynamic First Person rewrites view axis to limit character movement vs camera **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UDynamicFirstPerson* DynamicFirstPerson = nullptr;
	/** Speaker for character audio*/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UInteractiveObjectDialogueSpeaker* InteractiveObjectDialogueSpeaker = nullptr;
	/** Dynamic First Person rewrites view axis to limit character movement vs camera **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UCFH_CharacterActionHandler* CharacterActionHandler = nullptr;

	void AttachSubComponents();

public:
	// Sets default values for this actor's properties
	ACFH_PlayerCharacter();


	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	virtual void BeginPlay() override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
		
	// Handles moving forward/backward
	void MoveForward(float Val);

	// Handles stafing movement, left and right
	void MoveRight(float Val);

	// Called via input to turn at a given rate.
	// @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	void TurnAtRate(float Rate);

	// Called via input to turn look up/down at a given rate.
	// @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	void LookUpAtRate(float Rate);

	// Base turn rate, in deg/sec. Other scaling may affect final turn rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate{ 45.f };
	
	// Base look up/down rate, in deg/sec. Other scaling may affect final rate.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate{ 45.f };
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Animation")
	class UCFH_AnimInstance* AnimInst;

	FString ContextString;
};
