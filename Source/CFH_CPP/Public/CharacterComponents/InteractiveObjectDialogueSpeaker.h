// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/AudioComponent.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "InteractiveObjectDialogueSpeaker.generated.h"

/**
 * Handles dialogue audio directly related to interactive objects in the environment
 */

UCLASS( ClassGroup = (Custom), meta = (BlueprintSpawnableComponent) )
class CFH_CPP_API UInteractiveObjectDialogueSpeaker : public UAudioComponent
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	UInteractiveObjectDialogueSpeaker();

	void PlayObjectAudio(FString DialogueText, USoundWave* AudioClip, bool Override);

protected:
	void AddSubtitlesToAudio(FString DialogueText, USoundWave* AudioClip);
	void SetAndPlayAudio(USoundWave* AudioClip);
	void SetTimeValues(USoundWave* AudioClip);
	float LastSpeakTime = 0.f;
	float WaitToSpeakTime = 0.f;
 
};
