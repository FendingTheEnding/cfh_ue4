// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "InViewportHandler.generated.h"

// Forward Declaration
class ACFH_PlayerController;


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UInViewportHandler : public UActorComponent
{
	GENERATED_BODY()

private:	

	UPROPERTY()
	ACFH_PlayerController* MyController = nullptr;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	bool GetAndCheckRequiredClassPointers();
	TArray<float> GetCameraTraceAngles(FVector ObjectLocation);
	// Find Yaw of camera relative to object
	float CameraTraceYawAngle(FVector CameraTraceLocation, FRotator CameraTraceRotation, FVector ObjectLocation);
	// Find Pitch of camera relative to object
	float CameraTracePitchAngle(FVector CameraTraceLocation, FRotator CameraTraceRotation, FVector ObjectLocation);

public:	
	// Sets default values for this component's properties
	UInViewportHandler();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Gets Distance between view center and object location; Note: Camera is -22.5 in character relative x position
	bool ViewDistance(FVector ViewpointLocation, TArray<float> ObjectLocationRotation);
	// Finds if object is in view port based on angle of camera to object
	bool ObjectInViewport(FVector ObjectLocation);
	// Find distance between player and object
	bool ViewWithinDistance(FVector ObjectLocation, float InteractDistance);
};
