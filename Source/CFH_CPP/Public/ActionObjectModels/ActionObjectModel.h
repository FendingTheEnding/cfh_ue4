// Kenneth Kratzer 2020

#pragma once

// Included to allow reference of EActionType
#include "ActionSequenceHandler.h"
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ActionObjectModel.generated.h"

// Forward Declaration
class UActionGuideComponentHost;
class ACharacter;
class ACFH_PlayerCharacter;
class UCFH_CharacterActionHandler;
class UInViewportHandler;
class USoundWave;

// USTRUCT()
// Hold active ActionObject and Setter

UCLASS()
class CFH_CPP_API AActionObjectModel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AActionObjectModel();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void SetupSceneRootComponent();
	
	bool GetAndCheckRequiredClassPointers();
	
	// Base component so positioning of other components can change
	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* SceneRoot = nullptr;
	// Controls sequencing of actions; Sets active and what to look for next
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionSequenceHandler* ActionSequenceHandler = nullptr;
	// FIX: Controls sequencing of actions; Sets active and what to look for next
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionGuideComponentHost* ActionGuideComponentHost = nullptr;

	// Components referenced from this class
	UPROPERTY()
	ACharacter* MyCharacter = nullptr;
	UPROPERTY()
	UInViewportHandler* InViewportHandler = nullptr;
	UPROPERTY()
	UCFH_CharacterActionHandler* CharacterActionHandler = nullptr;

	// Assigned by subclass and used to select row of action guide needed
	UPROPERTY(EditAnywhere, Category = "Setup")
	FName ActionGuideName{ "" };

	// Reference to ActionSequenceHandler variable
	FActionSequenceHandlerMarkers* ActionMarkers;

	// Used as base check for if object can operate
	bool bEndStage{ true };
	// Standard time to wait inbetween each action
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float ActionBufferTime{ 0.2f };
	// Time SequenceActive was last true
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float DeactivationTime{ 0.f };

	// Called when ActionRequest is returned true
	void PreActionPrep();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Used to set Character position during animation; Set 1 for axis in line with object, actual position for other axis, and 0 for Z.
	UPROPERTY(EditAnywhere, Category = "Setup")
	FVector CharacterPositioning{ 0, 0, 0 };
	// Only uses Z value to adjust rotation character faces
	UPROPERTY(EditAnywhere, Category = "Setup")
	FRotator CharacterRotation{ 0, 0, 0 };
	// Set Y and Z to lock max/min values for look pitch and yaw during animation
	UPROPERTY(EditAnywhere, Category = "Setup")
	FRotator CharacterRotationOffset{ 0, 0, 0 };

	// Reference to ActionSequenceHandler variable
	FActionSequence* ActiveActionSequence;

	// Called from CharacterActionHandler; Checks ActionSequenceHandler for validity of requested action and plays action prep if true
	bool PlayerActionRequest(EActionType RequestedAction);
	// Called from CharacterActionHandler; Checks ActionSequenceHandler for validity of requested action and plays action prep if true
	bool ObjectActionRequest(EActionType RequestedAction);
	// Finds location animating character should have prior to animation being called
	FVector GetAnimationPositioning();
	// Hosts code to perform on animation; Left blank and to be used by sub-classes
	virtual void PerformOnAnim();
	// For use by subclasses; Will set render depth depending on main mesh type used by subclass
	virtual void SetRenderDepth(bool bValue) const;

	// Reference of Active Object for outside classes to reference
	static AActionObjectModel* ActiveActionObject;
	// Signals if an object is currently acting
	static bool bObjectInAction;

};

// Initialize Static Variables
bool AActionObjectModel::bObjectInAction = false;
AActionObjectModel* AActionObjectModel::ActiveActionObject = { nullptr };