// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "ActionObjectModel.h"
#include "StandardActionObject.generated.h"

// Forward Declaration
class UActionObjectVisualEffectsHandler;

UCLASS()
class CFH_CPP_API AStandardActionObject : public AActionObjectModel
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AStandardActionObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Allow player to interact with object
	void ActivateObject();
	// Turn off player ability to interact with object
	void DeactivateObject();
	// Set options such as visibility and object scan
	void CheckActiveSequenceOptions();
	void SetDeactivationTime();
	bool IsDeactivationTime();
	void ToggleVisibility();
	bool IsActionNPC();
	bool IsActionEndStage();
	bool IsObjectInSpeakDistance();
	// Signals if this object is currently acting
	bool bThisInAction{ false };
	// Signal for first tick of new sequence acting
	bool bFirstTickOfActiveSequence{ true };

	UPROPERTY(EditAnywhere, Category = "Setup")
	int32 SpeakDistance{ 275 };
	// Standard time to wait inbetween each action
	UPROPERTY(EditAnyWhere, Category = "Setup")
	float InactiveBufferTime{ 1.5f };


	/** Handles all visual effects on object **/
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionObjectVisualEffectsHandler* ActionObjectVisualEffectsHandler { nullptr };

	// Required for widget to work
	UFUNCTION(BlueprintImplementableEvent, Category = "Setup")
	void GetVisualEffectsHandler(UActionObjectVisualEffectsHandler* VisualEffectsHandlerCompRef);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;



};

