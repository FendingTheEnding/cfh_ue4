// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "StandardActionObject.h"
#include "StaticStandardActionObject.generated.h"

/**
 * 
 */
UCLASS()
class CFH_CPP_API AStaticStandardActionObject : public AStandardActionObject
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AStaticStandardActionObject();

	virtual void SetRenderDepth(bool bValue) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* StaticMeshObject = nullptr;
};
