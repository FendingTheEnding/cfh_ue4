// Kenneth Kratzer 2020

#pragma once

#include "CoreMinimal.h"
#include "StandardActionObject.h"
#include "AnimStandardActionObject.generated.h"

/**
 * 
 */
UCLASS()
class CFH_CPP_API AAnimStandardActionObject : public AStandardActionObject
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AAnimStandardActionObject();

	// Sets render depth true when object is to be scanned
	virtual void SetRenderDepth(bool bValue) const override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called by CharacterActionHandler when animation is executed
	virtual void PerformOnAnim() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USkeletalMeshComponent* SkeletalMeshObject = nullptr;
};
