// Kenneth Kratzer 2020

#pragma once
// Included to allow reference of Enums
#include "ActionSequenceHandler.h"
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionGuideComponentHost.generated.h"

// Forward Declaration
class UActionGuideRetriever;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionGuideComponentHost : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionGuideComponentHost();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	// Used to get Action Guide for object
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionGuideRetriever* ActionGuideRetriever { nullptr };
	// Controls sequencing of actions; Sets active and what to look for next
	UPROPERTY(VisibleAnywhere, Category = "Components")
	UActionSequenceHandler* ActionSequenceHandler { nullptr };

	FActionGuide* RetrieveActionGuide(FName ActionObjectName);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void HostSetup(FName ActionObjectName);

	UActionSequenceHandler* GetActionSequenceHandler();
};
