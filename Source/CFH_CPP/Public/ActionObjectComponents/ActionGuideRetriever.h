// Kenneth Kratzer 2020

#pragma once

// Included to allow reference of Enums
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionGuideRetriever.generated.h"

// Forward Declarations
class UActionObjectModel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionGuideRetriever : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionGuideRetriever();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Grabs action guide according to name of object and validates it before returning the guide to the action object
	FActionGuide* GetActionGuide(FName ActionObjectName, UMyGameInstanceCpp* GI);
};
