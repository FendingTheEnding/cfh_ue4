// Kenneth Kratzer 2020

#pragma once

// Included to allow reference of Enums
#include "MyGameInstanceCpp.h"

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ActionSequenceHandler.generated.h"

// Forward Declarations
class UDataTable;

USTRUCT(BlueprintType)
struct FActionSequenceHandlerMarkers
{
	GENERATED_BODY()

public:
	// Location of the currently active sequence in SequenceInformation array
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int32 ActiveSequenceIndex { 0 };
	// Array with value for each EActionType; Value set to location in SequenceInformation if type is a possible next sequence option
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<int32> ActionSequenceIndexes;
};

USTRUCT(BlueprintType)
struct FActiveHandlerSequenceActions
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bScannableObject{ false };
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bToggleObjectMarker{ false };
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString ActiveDialogue;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USoundWave* ActiveAudioClip;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence* ActiveAnimation;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimationAsset* ObjectAnimation;
};

USTRUCT(BlueprintType)
struct FSequenceHandlerReferences : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UActionSequenceHandler* HandlerReferenceArray;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class CFH_CPP_API UActionSequenceHandler : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UActionSequenceHandler();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Table to contain all ActionSequenceHandler Objects
	UDataTable* GetSequenceHandlerReferenceDT();

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Custom")
	UDataTable* SequenceHandlerReferenceDT;

	void SetActionGuideVariables(FActionGuide* ActionGuide);
	int32 HexIdentifier;
	TArray<FActionSequence> ActionSequences;
	void PopulateActionSequenceIndexesArray();
	void AddThisToSequenceHandlerReferenceDT();

	void SetNextSequenceOptions();
	void ResetValuesForNextSequence();
	void ResetActionMarkers();
	void SetActionTypeMarker(FString NextSequenceOption);
	void AssignMarkerForThis(int32 SequenceIndex);
	void AssignMarkerForExternal(int32 HexRequest, FString NextSequenceOption);

	void SetValuesForSequenceActionEnd(float ActionBufferTime);
	bool IsPlayerSequenceType();
	bool IsNPCSequenceType();
	bool IsAnimSequenceType();
	void SetSequenceDuration();
	void SetSequenceActive();

	bool IsRequestedActionValid(EActionType RequestedAction);
	bool IsAnimAction(EActionType RequestedAction);
	bool CheckDefaultSpeakTypes();
	void AssignActiveSequenceIndex(EActionType RequestedAction);

	// Set to time it takes for selected action to perform
	float SequenceDuration{ 0.f };
	// Allows multiple calls of RetargetSequenceAssignment from external object
	bool RetargetAssignment{ false };

	FActionSequenceHandlerMarkers ActionMarkers;
	FActionSequence ActiveActionSequence;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void SequenceHandlerSetup(FActionGuide* ActionGuide);
	// Used to re-assign NextSequenceOptions from external source
	void RetargetSequenceAssignment(FString RetargetSequence);
	// Prepares object for call of selected active sequence
	void SetActiveSequenceValues(float ActionBufferTime);
	// Sets timer to progress to next step after action has finished
	void PrepSequenceActive(float ActionBufferTime);
	
	// Called for Player ActionType requests
	bool PlayerActionRequest(EActionType RequestedAction, bool ObjectInViewport);
	// Called for NPC ActionType requests
	bool ObjectActionRequest(EActionType RequestedAction);

	FActionSequenceHandlerMarkers* GetActionSequenceHandlerMarkersPointer();
	FActionSequence* GetActiveActionSequencePointer();

	// Set true while Action is performing
	bool bSequenceActive{ false };
};
