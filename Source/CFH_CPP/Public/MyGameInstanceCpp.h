// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Runtime/Engine/Classes/Engine/DataTable.h"
#include "MyGameInstanceCpp.generated.h"

/**
 * 
 */


// Forward Declaration
class AActionObjectModel;
class UDataTable;


UENUM()
enum class EActionType : uint8
{
	StartStage,
	AggressiveAnim,
	PassiveAnim,
	PlayerSpeak,
	PlayerAggroSpeak,
	PlayerPassSpeak,
	NPCSpeak,
	NPCAggroSpeak,
	NPCPassSpeak,
	LevelWait,
	EndStage
};

USTRUCT(BlueprintType)
struct FActionSequence
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	EActionType SequenceType;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USoundWave* AudioReference;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString AudioSubtitles;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence* AnimationReference;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UAnimSequence* ObjectAnimationReference;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float ActionTimeExtension;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float TimeToBuffer;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	USoundWave* BufferAudioReference;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString BufferAudioSubtitles;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool ScanObjectOnAction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool ToggleObjectMarker;
	// Reference Number + Action Sequence Location; 'End' after final location
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FString> NextSequenceOptions;
};

USTRUCT(BlueprintType)
struct FActionGuide : public FTableRowBase
{
	GENERATED_BODY()

public:
	// Reference Number of Component in Hex
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	FString ReferenceNumber;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FActionSequence> SequenceInformation;
};

UCLASS()
class CFH_CPP_API UMyGameInstanceCpp : public UGameInstance
{
	GENERATED_BODY()

private:
	FString ContextString;
	UDataTable* GetActionGuideDT();

	// Table of objects and corresponding action sequences
	UPROPERTY(EditDefaultsOnly, Category = "Custom")
	UDataTable* ActionGuideDT;
	
public:
	UMyGameInstanceCpp(const FObjectInitializer& ObjectInitializer);

	// Returns action guide to each VPRA component
	FActionGuide* GetActionObjectSequence(FName ActionObjectName);
};
